package com.TNEB.Miniproj.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.TNEB.Miniproj.model.Newconsumer;
import com.TNEB.Miniproj.repo.Newconsumerrepo;

@Service
public class Newconsumerservice {

	@Autowired
	private Newconsumerrepo newconsumerrepo;

	public void newconsumerdata(Newconsumer newconsumer) {

		newconsumerrepo.save(newconsumer);

	}

	public List<Newconsumer> consumerName() {
		return newconsumerrepo.findAll();
	}

}
