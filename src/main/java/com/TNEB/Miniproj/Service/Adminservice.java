package com.TNEB.Miniproj.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.TNEB.Miniproj.model.Admin;
import com.TNEB.Miniproj.repo.Adminrepository;

@Service
public class Adminservice {
	
	@Autowired
	private Adminrepository adminrepository;
	
	public void admincredentialsdata( Admin a) {
		
		adminrepository.save(a);
		
	}

	public ResponseEntity<Admin> loginUser( Admin val){
		
		Admin created=adminrepository.findByusername(val.getUsername());
		if(created.getPassword().equals(val.getPassword())) 
			return ResponseEntity.ok(created);
		
				return (ResponseEntity<Admin>) ResponseEntity.internalServerError();
	}

	
	
	
	

}  
 