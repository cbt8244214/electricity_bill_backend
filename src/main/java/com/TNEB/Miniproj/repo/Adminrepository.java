package com.TNEB.Miniproj.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.TNEB.Miniproj.model.Admin;

@Repository
public interface Adminrepository extends JpaRepository<Admin, Integer>{
	
	Admin findByusername(String username);

	

}
