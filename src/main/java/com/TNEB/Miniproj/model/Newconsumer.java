package com.TNEB.Miniproj.model;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity

@Table(name="Newconsumer_table")
public class Newconsumer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="Username")
	private String username;
	@Column(name="Password")
	private String password;
	
	@Column(name="Address")
	private String address;
	@Column(name="Meter_Number")
	private String meterNumber;
	
	@Column(name="Email")
	private String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMeterNumber() {
		return meterNumber;
	}

	public void setMeterNumber(String meterNumber) {
		this.meterNumber = meterNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Newconsumer() {
		super();
	}

	public Newconsumer(int id, String username, String password, String address, String meterNumber, String email) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.address = address;
		this.meterNumber = meterNumber;
		this.email = email;
	}
	
	

	
	
	
	
	
	

}
