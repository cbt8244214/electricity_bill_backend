package com.TNEB.Miniproj.model;

public class DataPayload {
	 private String emailAddress;
     private String message;

     public String getEmailAddress() {
         return emailAddress;
     }

     public void setEmailAddress(String emailAddress) {
         this.emailAddress = emailAddress;
     }

     public String getMessage() {
         return message;
     }

     public void setMessage(String message) {
         this.message = message;
     }

}
