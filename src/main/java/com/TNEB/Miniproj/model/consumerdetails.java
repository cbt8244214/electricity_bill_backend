package com.TNEB.Miniproj.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class consumerdetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="Username")
	private String username;
	@Column(name="lastmodifieddate")
	private String lastmodifieddate;
	@Column(name="Consumption")
	private String consumption;
	@Column(name="Rupees")
	private int num;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getLastmodifieddate() {
		return lastmodifieddate;
	}
	public void setLastmodifieddate(String lastmodifieddate) {
		this.lastmodifieddate = lastmodifieddate;
	}
	public String getConsumption() {
		return consumption;
	}
	public void setConsumption(String consumption) {
		this.consumption = consumption;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public consumerdetails(int id, String username, String lastmodifieddate, String consumption, int num) {
		super();
		this.id = id;
		this.username = username;
		this.lastmodifieddate = lastmodifieddate;
		this.consumption = consumption;
		this.num = num;
	}
	public consumerdetails() {
		super();
	}
	
	
	
	
	
}
