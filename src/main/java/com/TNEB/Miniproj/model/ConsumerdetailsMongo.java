package com.TNEB.Miniproj.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;


 
 @Data
 @Document(collection="collection")
 
public class ConsumerdetailsMongo {
	

	private String username;
	private String lastmodifieddate;
	private String consumption;
	private int num;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getLastmodifieddate() {
		return lastmodifieddate;
	}
	public void setLastmodifieddate(String lastmodifieddate) {
		this.lastmodifieddate = lastmodifieddate;
	}
	public String getConsumption() {
		return consumption;
	}
	public void setConsumption(String consumption) {
		this.consumption = consumption;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public ConsumerdetailsMongo(String username, String lastmodifieddate, String consumption, int num) {
		super();
		this.username = username;
		this.lastmodifieddate = lastmodifieddate;
		this.consumption = consumption;
		this.num = num;
	}
	public ConsumerdetailsMongo() {
		super();
	}
	@Override
	public String toString() {
		return "ConsumerdetailsMongo [username=" + username + ", lastmodifieddate=" + lastmodifieddate
				+ ", consumption=" + consumption + ", num=" + num + "]";
	}

}
