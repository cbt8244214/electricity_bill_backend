package com.TNEB.Miniproj.model;

import lombok.Data;

@Data
public class EmailDto {

	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
