package com.TNEB.Miniproj.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.TNEB.Miniproj.Service.Adminservice;
import com.TNEB.Miniproj.Service.ConsumerService;
import com.TNEB.Miniproj.Service.Newconsumerservice;
import com.TNEB.Miniproj.model.Admin;
import com.TNEB.Miniproj.model.ConsumerdetailsMongo;
import com.TNEB.Miniproj.model.DataPayload;
import com.TNEB.Miniproj.model.Newconsumer;
import com.TNEB.Miniproj.model.consumerdetails;
import com.TNEB.Miniproj.repo.NewconsumerMongorepo;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200/")
public class Controller {
	
	@Autowired
	private Adminservice adminservice;
	@Autowired
	private Newconsumerservice newconsumerservice;
	@Autowired
	private ConsumerService consumerService;
	
	@Autowired
	private NewconsumerMongorepo newconsumerMongorepo;
	
	 @Autowired
	    private KafkaTemplate<String, String> kafkaTemplate;
	
//	@Autowired
//	private MailSenderService mailSenderService;
	
// for Admin credentials ...............................................................................	
	
	@PostMapping("/postadmincredentialsdata")
	public String saveadmincredentialsdata(@RequestBody Admin admin) {
		 adminservice.admincredentialsdata(admin);
		 return "done";
	}
	
	@PostMapping("/login")
	public ResponseEntity<Admin>login(@RequestBody Admin usercredentials){
		return this.adminservice.loginUser(usercredentials);
	}
	
	
//	for sql .............................................................................................
	
	@PostMapping("/postconsumerdetail")
	public consumerdetails consumerdetails(@RequestBody consumerdetails consumerdetails) {
		return consumerService.consumerdetail(consumerdetails);
		 
	}
	
	@PostMapping("/postnewconsumerdata")
	public String savenewconsumerdata(@RequestBody Newconsumer newconsumer) {
		newconsumerservice.newconsumerdata(newconsumer);
		 return "d";
	}
	
	@GetMapping("/getconsumername")
	public List<Newconsumer> consumername() {
		return newconsumerservice.consumerName();
		 
	}
	
	  
	
	// for mongo...................................................................................
	
	@GetMapping("/getconsumerdetailsmongo")
	public List<ConsumerdetailsMongo> getconsumerdetailmongo() {
		return newconsumerMongorepo.findAll();
		 
	}
	
	@PostMapping("/postconsumerdetailsmongo")
	public ConsumerdetailsMongo postconsumerdetailmongo(@RequestBody ConsumerdetailsMongo consumerdetailsMongo) {
		return newconsumerMongorepo.save(consumerdetailsMongo);
		 
	}
	
	 @PostMapping("/sendData")
	    public String sendDataToKafka(@RequestBody Newconsumer data) {
	        // Generate email using the data received
	        String emailContent = generateEmail(data);

	        // Send email content to Kafka topic
	        kafkaTemplate.send("Ayyappa", emailContent);
	        return "message published: ";
	    }
	 
	 private String generateEmail(Newconsumer data ) {
	        // Logic to generate email content for the provided email address and message
	        return " Consumer_Name:" + data.getUsername() + "\n" + "Consumer_Password:"+data.getPassword() +"\n"
	        		+ "Address:"+data.getAddress() +"\n"+"Meter_Number :"+data.getMeterNumber()+ "\n"+"Email_ID :"+data.getEmail();
	    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 // for kafka mail message.............................................................................
	
//	@PostMapping("/sendMail")
//	public void sendMesageTokafka(@RequestBody Newconsumer newconsumer ) {
//		consumerService.sendKafkaMail(newconsumer.getUsername(),newconsumer.getPassword(),newconsumer.getAddress(),newconsumer.getMeterNumber(),newconsumer.getEmail());
//
//	}
	
	
	

	
	

	

}
