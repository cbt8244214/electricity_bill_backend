package com.TNEB.Miniproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElectricityBillBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElectricityBillBackendApplication.class, args);
	}

}
